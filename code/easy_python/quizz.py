"""
Un jeu de quizz
Le programme pose des questions,
l'utilisateur répond dans la console,
le programme compare sa réponse avec la bonne réponse
le programme ajoute 1 point par bonne réponse
"""

# On commence avec aucun point
points = 0

question_reponses = [("Quelle est la capitale de la France ?", "Paris"),
      ("Quelle est la monnaie du Japon ?", "Yen"),
      ("Dans quelle ville ont eu lieu les Jeux Olympiques d'été en 2012 ?", "Londres"),
      ]

for i, (question, vraie_reponse) in enumerate(question_reponses):
    # Affiche la question
    print(i + 1, "-", question)
    # Récupère la réponse entrée par l'utilisateur dans une variable appelée capitale
    reponse = input()
    # Compare la réponse de l'utilisateur avec la bonne réponse, "Paris"
    if vraie_reponse.upper() in reponse.upper():
        # Donne un point si la réponse est bonne
        points += 1
        print("Bravo, bonne réponse !")
    else:
        print("Erreur ! La bonne réponse est :", vraie_reponse)

    if points > 1:
        print("Score :", points, "points")
    else:
        print("Score :", points, "point")


